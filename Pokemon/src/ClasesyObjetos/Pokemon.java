package ClasesyObjetos;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Jilver & Andres
 */

public class Pokemon {
    
    // Atributos POKEMON
    String nombrePO;
    int vidaPO;
    int ataquePO;

    //numero aleatorio
    Random Aleatorio = new Random();
    int ataque = 10;
    int vida = 50 + Aleatorio.nextInt(50);
    
    //POKEMONES
    public void pikachu(){
        Pokemon Pikachu = new Pokemon();
        
        Pikachu.nombrePO = "PIKACHU";
        Pikachu.ataquePO = ataque;
        Pikachu.vidaPO = vida;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 1 )\n"+
        "\n"+        
        "NOMBRE: [ "+Pikachu.nombrePO+" ]\n"+
        "ATAQUE: [ "+Pikachu.ataquePO+" ]\n"+
        "VIDA:   [ "+Pikachu.vidaPO+" ]"
        );  
    }
    
    public void bulbadaur(){
        Pokemon Bulbasaur = new Pokemon();
        
        Bulbasaur.nombrePO = "BULBASAUR";
        Bulbasaur.ataquePO = ataque;
        Bulbasaur.vidaPO = vida-27;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 2 )\n"+
        "\n"+         
        "NOMBRE: [ "+Bulbasaur.nombrePO+" ]\n"+
        "ATAQUE: [ "+Bulbasaur.ataquePO+" ]\n"+
        "VIDA:   [ "+Bulbasaur.vidaPO+" ]"
                
        );
    }
    
    public void squirtle(){
        Pokemon Squirtle = new Pokemon();
        
        Squirtle.nombrePO = "SQUIRTLE";
        Squirtle.ataquePO = ataque;
        Squirtle.vidaPO = vida-29;
        
        JOptionPane.showMessageDialog(null,
        
        "Pokemon numero ( 3 )\n"+
        "\n"+         
        "NOMBRE: [ "+Squirtle.nombrePO+" ]\n"+
        "ATAQUE: [ "+Squirtle.ataquePO+" ]\n"+
        "VIDA:   [ "+Squirtle.vidaPO+" ]"        
        
        );        
    }
    
    public void pidgey(){
        Pokemon Pidgey = new Pokemon();
        
        Pidgey.nombrePO = "PIDGEY";
        Pidgey.ataquePO = ataque;
        Pidgey.vidaPO = vida;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 1 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Pidgey.nombrePO+" ]\n"+
        "ATAQUE: [ "+Pidgey.ataquePO+" ]\n"+
        "VIDA:   [ "+Pidgey.vidaPO+" ]"
       
        );
    }
    
    public void snorlax(){
        Pokemon Snorlax = new Pokemon();
        
        Snorlax.nombrePO = "SNORLAX";
        Snorlax.ataquePO = ataque;
        Snorlax.vidaPO = vida-27;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 2 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Snorlax.nombrePO+" ]\n"+
        "ATAQUE: [ "+Snorlax.ataquePO+" ]\n"+
        "VIDA:   [ "+Snorlax.vidaPO+" ]"
       
        );
    }
    
    public void mew(){
        Pokemon Mew = new Pokemon();
        
        Mew.nombrePO = "NEW";
        Mew.ataquePO = ataque;
        Mew.vidaPO = vida-29;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 3 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Mew.nombrePO+" ]\n"+
        "ATAQUE: [ "+Mew.ataquePO+" ]\n"+
        "VIDA:   [ "+Mew.vidaPO+" ]"
       
        );
    }
    
    public void caterpie(){
        Pokemon Caterpie = new Pokemon();
        
        Caterpie.nombrePO = "CATERPIE";
        Caterpie.ataquePO = ataque;
        Caterpie.vidaPO = vida;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 1 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Caterpie.nombrePO+" ]\n"+
        "ATAQUE: [ "+Caterpie.ataquePO+" ]\n"+
        "VIDA:   [ "+Caterpie.vidaPO+" ]"
       
        );
    }
    
    public void dratini(){
        Pokemon Dratini = new Pokemon();
        
        Dratini.nombrePO = "DRATINI";
        Dratini.ataquePO = ataque;
        Dratini.vidaPO = vida-27;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 2 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Dratini.nombrePO+" ]\n"+
        "ATAQUE: [ "+Dratini.ataquePO+" ]\n"+
        "VIDA:   [ "+Dratini.vidaPO+" ]"
       
        );
    }
    
    public void eevee(){
        Pokemon Eevee = new Pokemon();
        
        Eevee.nombrePO = "EEVEE";
        Eevee.ataquePO = ataque;
        Eevee.vidaPO = vida-29;
        
        JOptionPane.showMessageDialog(null,
         
        "Pokemon numero ( 3 )\n"+        
        "\n"+        
        "NOMBRE: [ "+Eevee.nombrePO+" ]\n"+
        "ATAQUE: [ "+Eevee.ataquePO+" ]\n"+
        "VIDA:   [ "+Eevee.vidaPO+" ]"
       
        );
    }
    
}
