package ClasesyObjetos;

import javax.swing.JOptionPane;

/**
 *
 * @author Jilver & Andres
 */

public class Seleccion {
    
    public void EntreAhs(){
       
        JOptionPane.showMessageDialog(null, "Usted ha seleccionado a AHS.\n");
        
        //LLAMADO POKEMONES
        Pokemon pikachu = new Pokemon();
        pikachu.pikachu();
        
        Pokemon bulbadaur = new Pokemon();
        bulbadaur.bulbadaur();
        
        Pokemon squirtle = new Pokemon();
        squirtle.squirtle();
              
    }
    
    public void EntreMisty(){
        JOptionPane.showMessageDialog(null, "Usted ha seleccionado a MISTY.\n");   
        
        //LLAMADO POKEMONES
        Pokemon pidgey = new Pokemon();
        pidgey.pidgey();
        
        Pokemon snorlax = new Pokemon();
        snorlax.snorlax();
        
        Pokemon mew = new Pokemon();
        mew.mew();
                
    }
    
   public void EntreBrock(){
        JOptionPane.showMessageDialog(null, "Usted ha seleccionado a BROCK.\n");
        
        Pokemon caterpie = new Pokemon();
        caterpie.caterpie();
        
        Pokemon dratini = new Pokemon();
        dratini.dratini();
        
        Pokemon eevee = new Pokemon();
        eevee.eevee();
   } 
     
}
